import React, { Component } from "react";
import "./App.css";
import { Row, Col } from "react-bootstrap";
import { BrowserRouter as Router, Route } from "react-router-dom";

import Login from "./components/Login";
import Profile from "./components/Profile";
import Navigation from "./components/Navigation";

class App extends Component {
  render() {
    return (
      <Router>
        <div className="container">
          <Row>
            <Col mdPush={3} md={6}>
              <Navigation />
              <Route exact path="/" component={Login} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/profile" component={Profile} />
            </Col>
          </Row>
        </div>
      </Router>
    );
  }
}

export default App;
