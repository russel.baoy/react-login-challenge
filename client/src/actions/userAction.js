import axios from "axios";

export function setUserInfo(data) {
  return {
    type: "SET_USER_INFO",
    payload: data
  };
}
export function retrieveUserInfo() {
  return function(dispatch) {
    axios
      .get("api/user-info", {
        params: { token: localStorage.getItem("token") }
      })
      .then(res => {
        dispatch(setUserInfo(res.data));
      });
  };
}
