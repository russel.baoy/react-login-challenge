import axios from "axios";

export function toggleRememberMe() {
  return {
    type: "TOGGLE_REMEMBER_ME"
  };
}

export function changeUsername(value) {
  return {
    type: "CHANGE_USERNAME",
    payload: value
  };
}

export function changePassword(value) {
  return {
    type: "CHANGE_PASSWORD",
    payload: value
  };
}

export function changeUsernameValidation(value) {
  return {
    type: "CHANGE_USERNAME_VALIDATION",
    payload: value
  };
}

export function changePasswordValidation(value) {
  return {
    type: "CHANGE_PASSWORD_VALIDATION",
    payload: value
  };
}

export function setLoginError(value = "Invalid Login.") {
  return {
    type: "SET_LOGIN_ERROR",
    payload: value
  };
}

export function submitLoginForm(username, password, rememberMe, history) {
  return function(dispatch) {
    axios
      .post("api/login", {
        username: username,
        password: password
      })
      .then(res => {
        localStorage.setItem("token", res.data.token);
        history.push("/profile");
      })
      .catch(err => {
        console.log(err);
        dispatch(setLoginError());
      });
  };
}
