import { connect } from "react-redux";
import { changeUsername, changePassword } from "../actions/userAction";
import { Login } from "../components/Login";

const mapStateToProps = state => {
  return {
    login: state.login
  };
};

const maptDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  maptDispatchToProps
)(Login);
