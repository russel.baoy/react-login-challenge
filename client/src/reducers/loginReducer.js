export default function reducer(
  state = {
    username: "",
    password: "",
    rememberMe: false,
    loginError: null,
    usernameValidation: null,
    passwordValidation: null
  },
  action
) {
  switch (action.type) {
    case "TOGGLE_REMEMBER_ME":
      return { ...state, rememberMe: !state.rememberMe };
    case "CHANGE_USERNAME":
      return { ...state, username: action.payload };
    case "CHANGE_PASSWORD":
      return { ...state, password: action.payload };
    case "CHANGE_USERNAME_VALIDATION":
      return { ...state, usernameValidation: action.payload };
    case "CHANGE_PASSWORD_VALIDATION":
      return { ...state, passwordValidation: action.payload };
    case "SET_LOGIN_ERROR":
      return { ...state, loginError: action.payload };
    default:
      return state;
  }
}
