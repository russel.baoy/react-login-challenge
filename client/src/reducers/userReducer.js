export default function reducer(
  state = {
    name: "",
    age: "",
    gender: "",
    address: ""
  },
  action
) {
  switch (action.type) {
    case "SET_USER_INFO":
      return { ...state, ...action.payload };
    default:
      return state;
  }
}
