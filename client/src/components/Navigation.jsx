import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { Col, Navbar, Nav, NavItem } from "react-bootstrap";

class Navigation extends Component {
  logOut(e) {
    e.preventDefault();
    localStorage.removeItem("token");
    this.props.history.push(`/`);
  }

  profile(e) {
    e.preventDefault();
    this.props.history.push(`/profile`);
  }

  render() {
    if (!localStorage.getItem("token")) return <Navbar />;

    return (
      <Navbar>
        <Nav>
          <NavItem eventKey={2} href="#" onClick={this.profile.bind(this)}>
            Profile
          </NavItem>
          <NavItem eventKey={3} href="#" onClick={this.logOut.bind(this)}>
            Logout
          </NavItem>
        </Nav>
      </Navbar>
    );
  }
}

export default withRouter(Navigation);
