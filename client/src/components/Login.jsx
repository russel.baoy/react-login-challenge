import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import {
  changeUsername,
  changePassword,
  submitLoginForm,
  changeUsernameValidation,
  changePasswordValidation,
  toggleRememberMe
} from "../actions/loginAction";

import {
  Col,
  FormGroup,
  FormControl,
  ControlLabel,
  Checkbox,
  Button,
  Alert
} from "react-bootstrap";

class Login extends Component {
  constructor() {
    super();
    this.handeSubmit = this.handeSubmit.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.handleChangeUsername = this.handleChangeUsername.bind(this);
    this.handleToggleRememberMe = this.handleToggleRememberMe.bind(this);
  }

  componentDidMount() {
    if (localStorage.getItem("token")) this.props.history.push("/profile");
  }

  handleChangeUsername(e) {
    const value = e.target.value;
    const { onChangeUsernameValidation, onChangeUsername } = this.props;
    let validation = "success";

    if (!value) {
      validation = "error";
    }

    onChangeUsernameValidation(validation);
    onChangeUsername(value);
  }

  handleChangePassword(e) {
    const value = e.target.value;
    const { onChangePasswordValidation, onChangePassword } = this.props;
    let validation = "success";

    if (!value) {
      validation = "error";
    }

    onChangePasswordValidation(validation);
    onChangePassword(e.target.value);
  }

  handleToggleRememberMe(e) {
    this.props.onToggleRememberMe();
  }

  handeSubmit(e) {
    e.preventDefault();
    const {
      username,
      password,
      rememberMe,
      history,
      onSubmitLoginForm
    } = this.props;
    onSubmitLoginForm(username, password, rememberMe, history);
  }

  render() {
    const {
      username,
      password,
      usernameValidation,
      passwordValidation,
      loginError
    } = this.props;

    const loginErrorMessage = loginError ? (
      <Alert bsStyle="warning">{loginError}</Alert>
    ) : (
      ""
    );

    return (
      <form className="form-horizontal" onSubmit={this.handeSubmit}>
        {loginErrorMessage}

        <FormGroup
          controlId="usernameFormControl"
          validationState={usernameValidation}
        >
          <Col componentClass={ControlLabel} sm={2}>
            Username
          </Col>
          <Col sm={10}>
            <FormControl
              type="text"
              value={username}
              onChange={this.handleChangeUsername}
              placeholder="your@email.com"
            />
            <FormControl.Feedback />
          </Col>
        </FormGroup>
        <FormGroup
          controlId="passwordFormControl"
          validationState={passwordValidation}
        >
          <Col componentClass={ControlLabel} sm={2}>
            Password
          </Col>
          <Col sm={10}>
            <FormControl
              type="password"
              value={password}
              onChange={this.handleChangePassword}
              placeholder="password"
            />
            <FormControl.Feedback />
          </Col>
        </FormGroup>
        <FormGroup controlId="rememberMeFormControl">
          <Col smOffset={2} sm={10}>
            <Checkbox onChange={this.handleToggleRememberMe}>
              Remember me
            </Checkbox>
          </Col>
        </FormGroup>

        <FormGroup controlId="submitFormControl">
          <Col smOffset={2} sm={10}>
            <Button
              bsStyle="primary"
              type="submit"
              disabled={!(password && username)}
            >
              Sign In
            </Button>
          </Col>
        </FormGroup>
      </form>
    );
  }
}

const mapStateToProps = state => {
  return { ...state.login };
};

const mapDispatchToProps = dispatch => {
  return {
    onChangeUsername: value => dispatch(changeUsername(value)),
    onChangePassword: value => dispatch(changePassword(value)),
    onChangeUsernameValidation: value =>
      dispatch(changeUsernameValidation(value)),
    onChangePasswordValidation: value =>
      dispatch(changePasswordValidation(value)),
    onToggleRememberMe: () => dispatch(toggleRememberMe),
    onSubmitLoginForm: (username, password, rememberMe, history) =>
      dispatch(submitLoginForm(username, password, rememberMe, history))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Login));
