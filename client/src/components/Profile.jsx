import React, { Component } from "react";
import { connect } from "react-redux";
import { Col, Panel } from "react-bootstrap";
import { withRouter } from "react-router";
import { retrieveUserInfo } from "../actions/userAction";

class Profile extends Component {
  componentDidMount() {
    if (!localStorage.getItem("token")) this.props.history.push("/");
    this.props.retrieveUserInfo();
  }

  render() {
    const { name, age, gender, address } = this.props;
    return (
      <Panel>
        <Panel.Body>
          <Col>Name: {name}</Col>
          <Col>Age: {age}</Col>
          <Col>Gender: {gender}</Col>
          <Col>Address: {address}</Col>
        </Panel.Body>
      </Panel>
    );
  }
}

const mapStateToProps = state => {
  return { ...state.user };
};

const mapDispatchToProps = dispatch => {
  return {
    retrieveUserInfo: () => dispatch(retrieveUserInfo())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Profile));
