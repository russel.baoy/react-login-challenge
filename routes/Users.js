const express = require("express");
const users = express.Router();
const config = require("../config");

users.post("/login", (req, res) => {
  if (
    config.username === req.body.username &&
    config.password === req.body.password
  ) {
    res.send({ token: config.token });
  } else {
    res.status(401).send("Authentacation failed.");
  }
});

users.get("/user-info", (req, res) => {
  res.send(config.profile);
});

module.exports = users;
