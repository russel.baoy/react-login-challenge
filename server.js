var express = require("express");
var app = express();

var bodyParser = require("body-parser");
var port = process.env.PORT || 5000;

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: false
  })
);

var Users = require("./routes/Users");
app.use("/api", Users);

app.listen(port, () => {
  console.log("Server is running on port: " + port);
});
